package tr.gov.yok.authserver.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.config.BeanIds;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.builders.WebSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.web.savedrequest.NullRequestCache;
import tr.gov.yok.authserver.security.CustomUserDetailsService;
import tr.gov.yok.authserver.security.JwtAuthenticationEntryPoint;
import tr.gov.yok.authserver.security.JwtTokenProvider;





@Configuration
@EnableWebSecurity
@EnableGlobalMethodSecurity(securedEnabled = true, jsr250Enabled = true,prePostEnabled = true)
public class SecurityConf extends WebSecurityConfigurerAdapter {

	
	@Autowired
	private JwtAuthenticationEntryPoint unauthorizedHandler;
	@Autowired
	CustomUserDetailsService customUserDetailsService;
	@Autowired
	JwtTokenProvider jwtTokenProvider;
	
	@Override
	public void configure(AuthenticationManagerBuilder auth) throws Exception {
		auth.userDetailsService(customUserDetailsService).passwordEncoder(passwordEncoder());
	}
	
	   @Override
	    protected void configure(HttpSecurity http) throws Exception {

	        // Disable CSRF (cross site request forgery)
	      http.csrf().disable();
	        
	        //eklemeler
	        http.cors().disable();//.configurationSource(corsConfigurationSource());
	        http.exceptionHandling().authenticationEntryPoint(unauthorizedHandler);
	        http.requestCache().requestCache(new NullRequestCache());
	        http.httpBasic();
	        http.apply(new JwtConfigurer(jwtTokenProvider));
	        http.authorizeRequests().antMatchers("/", "/favicon.ico", "/**/*.png", "/**/*.gif", "/**/*.svg", "/**/*.jpg", "/**/*.html", "/**/*.css", "/**/*.js").permitAll();
	        
	        // No session will be created or used by spring security
	 //       http.sessionManagement().sessionCreationPolicy(SessionCreationPolicy.STATELESS);

	        // Entry points
	        http.authorizeRequests()
	                .antMatchers("/signin/**").permitAll()
	                // Disallow everything else..
	                .anyRequest().authenticated();
	        http.authorizeRequests().antMatchers( "/api/unit/delete/**").hasAuthority("ROLE_ADMIN");	
	        http.authorizeRequests().antMatchers( "/api"+"/unit"+"/unitAttach/**").hasAuthority("ROLE_ADMIN");
	        // If a user try to access a resource without having enough permissions
	        http.exceptionHandling().accessDeniedPage("/login");

//	         Apply JWT
//	        http.apply(new JwtTokenFilterConfigurer(jwtTokenProvider));

	        // Optional, if you want to test the API from a browser
	        // http.httpBasic();
	    }

	    @Override
	    public void configure(WebSecurity web) throws Exception {

	        web.ignoring()//.antMatchers("/*/")//
	                .antMatchers("/eureka/**");
	             //   .antMatchers(HttpMethod.OPTIONS, "/**");
	    }

	    @Bean
	    public PasswordEncoder passwordEncoder() {
	        return new BCryptPasswordEncoder();
	    }

		@Bean(BeanIds.AUTHENTICATION_MANAGER)
		@Override
		public AuthenticationManager authenticationManagerBean() throws Exception {
			return super.authenticationManagerBean();
		}
		
//		@Bean
//		CorsConfigurationSource corsConfigurationSource() {
//			CorsConfiguration configuration = new CorsConfiguration();
//			configuration.setAllowedOrigins(Arrays.asList("*"));
//			configuration.setAllowedMethods(Arrays.asList("*"));
//			configuration.setAllowedHeaders(Arrays.asList("*"));
//			configuration.setAllowCredentials(true);
//			UrlBasedCorsConfigurationSource source = new UrlBasedCorsConfigurationSource();
//			source.registerCorsConfiguration("/**", configuration);
//			return source;
//		}
}
