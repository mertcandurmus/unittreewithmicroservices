import { Injectable } from '@angular/core';
import { BasicauthenticationService } from '../basicauthentication.service';
import { HttpRequest, HttpHandler, HttpEvent, HttpInterceptor, HttpResponse } from '@angular/common/http';
import { Observable, throwError } from 'rxjs';
import { map, catchError } from 'rxjs/operators';
import { CookieService } from 'ngx-cookie-service';


//  Bu servis BasicAuthenticationService işleminden sonra login olunmuşsa ilgili user  ve token bilgilerini
//  Servise header olarak göndermekte ve veri alışverişi için izin almakta

/*Intercepter kullanmamızın nedeni
Başvurunuzda, uygulamanıza sunucuya yapılan tüm istekleri kontrol edip 
sunucudan gelen tüm yanıtları kontrol edebileceğiniz ortak bir yer istiyorsanız, 
en iyi yöntem INTERCEPTOR kullanmaktır.
*/


@Injectable({
  providedIn: 'root'
})
export class HttpIntercepterBasicAuthService implements HttpInterceptor {

  constructor(private basicAuthService: BasicauthenticationService, private cookieService: CookieService) { }





  intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    const basicAuthHeaderString = this.basicAuthService.getAuthenticatedToken();
    const username = this.basicAuthService.getAuthenticatedUser();

    if (basicAuthHeaderString && username) {

    request = request.clone({  withCredentials: true,
      setHeaders : {
        Authorization : basicAuthHeaderString
      }

      });
    }
    return next.handle(request)
    .pipe(
        map((event: HttpEvent<any>) => {
            if (event instanceof HttpResponse) {
                console.log('Http Response event: ', event);
            }
            return event;
        }),
        catchError(error => {
            console.log('Error response status: ', error.status);
            if (error.status === 401) {
              console.log('Error response status: ', error.status);
            }
            return throwError(error);
        }));

}



}
