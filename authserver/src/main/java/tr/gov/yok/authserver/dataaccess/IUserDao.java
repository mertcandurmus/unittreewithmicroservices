package tr.gov.yok.authserver.dataaccess;

import tr.gov.yok.authserver.entity.User;

public interface IUserDao {

	void add(User user);

	void delete(User user);

	void update(User user);

}
