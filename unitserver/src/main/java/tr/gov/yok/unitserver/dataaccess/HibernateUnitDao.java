package tr.gov.yok.unitserver.dataaccess;

import java.util.List;

import javax.persistence.EntityManager;

import org.hibernate.Session;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import tr.gov.yok.unitserver.entities.Unit;
import tr.gov.yok.unitserver.entities.UnitType;

@Repository
public class HibernateUnitDao implements IUnitDao {

	private EntityManager entityManager;

	@Autowired
	public HibernateUnitDao(EntityManager entityManager) {

		this.entityManager = entityManager;
	}

	@Override
	public List<Unit> getAll() {
		Session session = entityManager.unwrap(Session.class);
		List<Unit> units = session.createQuery("from Unit", Unit.class).getResultList();
		for (int i = 0; i < units.size(); i++) {
			int typeID = units.get(i).getUnitType().getId();
			String type = getType2(typeID);
			units.get(i).setType(type);
		}
		return units;
	}

	@Override
	@Transactional
	public void add(Unit unit) {
		Session session = entityManager.unwrap(Session.class);
		session.save(unit);
	}

	@Override
	@Transactional
	public void delete(Unit unit) {
		Session session = entityManager.unwrap(Session.class);
		Unit deleteUnit = session.get(Unit.class, unit.getId());
		session.delete(deleteUnit);
	}

	@Override
	@Transactional
	public void update(Unit unit) {
		Session session = entityManager.unwrap(Session.class);
		session.saveOrUpdate(unit);
	}

	@Override
	@Transactional
	public Unit getById(int id) {
		Session session = entityManager.unwrap(Session.class);
		Unit unit = session.get(Unit.class, id);
		int typeID = unit.getUnitType().getId();
		String type = getType2(typeID);
		unit.setType(type);
		return unit;
	}

	@Override
	@Transactional
	public List<Unit> getAttechedUnit(int attachedunit) {
		Session session = entityManager.unwrap(Session.class);
		String hql = "FROM Unit E WHERE E.attachedunit = :attach_id";
		@SuppressWarnings({ "unchecked" })
		Query<Unit> query = session.createQuery(hql);
		query.setParameter("attach_id", attachedunit);
		List<Unit> units = query.list();
		for (int i = 0; i < units.size(); i++) {
			int typeID = units.get(i).getUnitType().getId();
			String type = getType2(typeID);
			units.get(i).setType(type);
		}
		return units;
	}

	@Override
	public String getType(int typeid) {
		Session session = entityManager.unwrap(Session.class);
		String hql = "SELECT namee FROM Unit E JOIN UnitType ET WHERE ET.id = :type_id";
		@SuppressWarnings("unchecked")
		Query<Unit> query = session.createQuery(hql);
		query.setParameter("type_id", typeid);
		String cntr = query.toString();
		return cntr;
	}

	@Override
	public String getType2(int id) {
		Session session = entityManager.unwrap(Session.class);
		UnitType unitType = session.get(UnitType.class, id);
		String name = unitType.getNamee();
		return name;
	}

	public List<UnitType> getAllType() {
		Session session = entityManager.unwrap(Session.class);
		List<UnitType> allType = session.createQuery("from UnitType", UnitType.class).getResultList();
		return allType;
	}

	@Override
	public List<Unit> getByType(String type) {
		Session session = entityManager.unwrap(Session.class);
		String hql = "FROM Unit E WHERE E.type = :type_id";
		Query<Unit> query = session.createQuery(hql);		
		query.setParameter("type_id", type);
		List<Unit> unitstype = query.list();
		return unitstype;
	}

}
