import { Component, OnInit } from '@angular/core';
import { Unit } from '../models/unit';
import { UnitService } from '../services/unit.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-create-unit',
  templateUrl: './create-unit.component.html',
  styleUrls: ['./create-unit.component.css']
})
export class CreateUnitComponent implements OnInit {

  constructor(private unitService: UnitService, private router: Router) { }

  unit: Unit[];
  yksId = 'a';
  leastUnit: Unit[];

  ngOnInit() {
    this.getLeastUnit(0);

  }

  createUnit(unit: Unit): void {
    window.localStorage.removeItem('cUnitId');
    window.localStorage.setItem('cUnitId', unit.id.toString());
    this.router.navigate(['unitCreate']);
  }
  createUnitY(unit: Unit): void {
    window.localStorage.removeItem('cUnitId');
    window.localStorage.setItem('cUnitId', this.yksId.toString());
    this.router.navigate(['unitCreate']);
  }

  getLeastUnit(id: number) {
    this.unitService.getLeastUnit(id).subscribe(data => {
      this.leastUnit = data;
    });
  }
}
