import { BrowserModule } from '@angular/platform-browser';
import { NgModule, Injectable } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { BirimEkleComponent } from './birim-ekle/birim-ekle.component';
import { BirimSilComponent } from './birim-sil/birim-sil.component';
import { BirimGuncelleComponent } from './birim-guncelle/birim-guncelle.component';
import { BirimGosterComponent } from './birim-goster/birim-goster.component';
import { UnitService } from './services/unit.service';
import { HttpClientModule, HTTP_INTERCEPTORS, HttpClientXsrfModule } from '@angular/common/http';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { UpdateFormComponent } from './update-form/update-form.component';
import { CreateUnitComponent } from './create-unit/create-unit.component';
import { ConfirmDialogComponent } from './confirm-dialog/confirm-dialog.component';
import { ConfirmDialogService } from './services/confirm-dialog.service';
import { LoginComponent } from './login/login.component';
import { LogoutComponent } from './logout/logout.component';
import { RegisterComponent } from './register/register.component';
import { HttpIntercepterBasicAuthService } from './services/http/http-intercepter-basic-auth.service';
import { CookieService } from 'ngx-cookie-service';



@NgModule({
  declarations: [
    AppComponent,
    BirimEkleComponent,
    BirimSilComponent,
    BirimGuncelleComponent,
    BirimGosterComponent,
    UpdateFormComponent,
    CreateUnitComponent,
    ConfirmDialogComponent,
    LoginComponent,
    LogoutComponent,
    RegisterComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    FormsModule,
    ReactiveFormsModule,
  //  HttpClientXsrfModule.withOptions({cookieName: 'XSRF-TOKEN'})




  ],
  providers: [UnitService, ConfirmDialogService, {
    provide: HTTP_INTERCEPTORS,
    useClass: HttpIntercepterBasicAuthService,
    multi: true
  }, CookieService],
  bootstrap: [AppComponent]
})
export class AppModule { }


