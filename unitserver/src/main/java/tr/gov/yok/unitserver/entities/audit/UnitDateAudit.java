package tr.gov.yok.unitserver.entities.audit;

import java.io.Serializable;
import java.time.Instant;

import javax.persistence.Column;
import javax.persistence.EntityListeners;
import javax.persistence.MappedSuperclass;

import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedDate;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@MappedSuperclass
@EntityListeners(AuditingEntityListener.class)
@JsonIgnoreProperties(value = { "cdate", "udate" }, allowGetters = true)
public class UnitDateAudit implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 7088636650613181224L;

	@CreatedDate
	@Column(nullable = false, updatable = false)
	private Instant cdate;

	@LastModifiedDate
	@Column(nullable = false)
	private Instant udate;

	public Instant getCdate() {
		return cdate;
	}

	public void setCdate(Instant cdate) {
		this.cdate = cdate;
	}

	public Instant getUdate() {
		return udate;
	}

	public void setUdate(Instant udate) {
		this.udate = udate;
	}
}
