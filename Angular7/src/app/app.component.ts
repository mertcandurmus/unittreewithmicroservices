import { Component, OnInit } from '@angular/core';
import { BasicauthenticationService } from './services/basicauthentication.service';
import { Router } from '@angular/router';
import { CookieService } from 'ngx-cookie-service';


@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css'],
})
export class AppComponent implements OnInit {

  title = 'FrontEndUnitTree';
  authenticated;
  user;

  constructor(private router: Router,
              private basicAuthenticationService: BasicauthenticationService
              // tslint:disable-next-line:align
              , private cookieService: CookieService) { }

              ngOnInit() {
              this.authenticated = this.basicAuthenticationService.isUserLoggedIn();
              this.user = this.basicAuthenticationService.getAuthenticatedUser();
              }

control() {
  this.router.navigate(['login']);
 // this.basicAuthenticationService.isUserLoggedIn();
 // setTimeout(() => {this.authenticated = true; this.control3(); }, 3500);
}


control2() {
  this.router.navigate(['logout']);
 // this.basicAuthenticationService.isUserLoggedIn();
 // setTimeout(() => {this.authenticated = false; this.control3(); }, 3500);
}

}
