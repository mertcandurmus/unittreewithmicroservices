import { Component, OnInit } from '@angular/core';
import { Unit } from '../models/unit';
import { UnitService } from '../services/unit.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-birim-guncelle',
  templateUrl: './birim-guncelle.component.html',
  styleUrls: ['./birim-guncelle.component.css']
})
export class BirimGuncelleComponent implements OnInit {




  constructor(private unitService: UnitService, private router: Router) { }

  unit: Unit[];
  leastUnit: Unit[];


  ngOnInit() {
    this.getLeastUnit(0);
  //  this.unitService.getAuth();


  }



  editUnit(unit: Unit): void {
    window.localStorage.removeItem('editUnitId');
    window.localStorage.setItem('editUnitId', unit.id.toString());
    this.router.navigate(['updateUnit']);
  }


  reloadData() {
    this.unitService.getUnits().subscribe(data => {
      this.unit = data;
    });
  }

  getLeastUnit(id: number) {
    this.unitService.getLeastUnit(id).subscribe(data => {
      this.leastUnit = data;
    });
  }
}
