import { Injectable } from '@angular/core';
import { Router, NavigationStart } from '@angular/router';
import { Observable } from 'rxjs';
import { Subject } from 'rxjs';
@Injectable() export class ConfirmDialogService {
    private subject = new Subject<any>();
    constructor() { }
    ctrl;
    confirmThis(message: string, siFn: () => void, noFn: () => void) {
        this.setConfirmation(message, siFn, noFn);
    }
    setConfirmation(message: string, siFn: () => void, noFn: () => void) {
        const that = this;
        this.subject.next({
            type: 'confirm',
            text: message,
            siFn() {
                    that.subject.next();
                    this. ctrl = true;
                    siFn();
                },
            noFn() {
                that.subject.next();
                this. ctrl = false;
                noFn();
            }
        });
    }
    getMessage(): Observable<any> {
        return this.subject.asObservable();
    }
}